<?php

namespace Drupal\facet_active_by_default\EventSubscriber;

use Drupal\Core\Url;
use Drupal\redirect\Entity\Redirect;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscribers for the Facet active by default module.
 */
final class FacetActiveByDefaultSubscriber implements EventSubscriberInterface {

  /**
   * Kernel response event handler.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    if ($facet_active_path = $event->getRequest()->attributes->get('facet_active_by_default', FALSE)) {
      $event->setResponse(new RedirectResponse(Url::fromUserInput($facet_active_path)->toString()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

}
