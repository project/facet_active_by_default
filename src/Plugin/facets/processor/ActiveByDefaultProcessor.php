<?php

namespace Drupal\facet_active_by_default\Plugin\facets\processor;

use Drupal\Core\DrupalKernelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a processor that makes the first facet active by default.
 *
 * @FacetsProcessor(
 *   id = "make_first_facet_active",
 *   label = @Translation("Make first facet active"),
 *   description = @Translation("If there is no facet selected, the first facet will automatically be selected"),
 *   stages = {
 *     "build" = 55
 *   }
 * )
 */
class ActiveByDefaultProcessor extends ProcessorPluginBase implements BuildProcessorInterface, ContainerFactoryPluginInterface {

  /**
   * The Drupal kernel.
   *
   * @var \Drupal\Core\DrupalKernelInterface
   */
  protected DrupalKernelInterface $kernel;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs an ActiveByDefaultProcessor object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\DrupalKernelInterface $kernel
   *   The Drupal kernel.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DrupalKernelInterface $kernel, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->kernel = $kernel;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('kernel'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    /** @var \Drupal\facets\Result\ResultInterface $result */
    foreach ($results as $result) {
      if ($result->isActive()) {
        return $results;
      }
    }

    // Trigger sort stage. We have to do this ourselves because build processors
    // don't have any way to run after the sort has happened, and sort processors
    // don't have access to the entire result set after it's been sorted.
    $active_sort_processors = [];
    foreach ($facet->getProcessorsByStage(ProcessorInterface::STAGE_SORT) as $processor) {
      $active_sort_processors[] = $processor;
    }

    // Sort the actual results if we have enabled sort processors.
    if (!empty($active_sort_processors)) {
      $results = $this->sortFacetResults($active_sort_processors, $results);
    }

    $first_result = array_key_first($results);

    if ($first_result !== NULL) {
      $result_url = $results[$first_result]->getUrl()->toString();
      $request = $this->requestStack->getCurrentRequest();
      $request->attributes->set('facet_active_by_default', $result_url);
    }

    return $results;
  }

  /**
   * This is a straight copy of \Drupal\facets\FacetManager\DefaultFacetManager::sortFacetResults
   *
   * We need the same functionality but it's protected on that class and this
   * allows us to avoid having to get too intrusive.
  **/
  protected function sortFacetResults(array $active_sort_processors, array $results) {
    uasort($results, function ($a, $b) use ($active_sort_processors) {
      $return = 0;
      foreach ($active_sort_processors as $sort_processor) {
        if ($return = $sort_processor->sortResults($a, $b)) {
          if ($sort_processor->getConfiguration()['sort'] == 'DESC') {
            $return *= -1;
          }
          break;
        }
      }
      return $return;
    });

    // Loop over the results and see if they have any children, if they do, fire
    // a request to this same method again with the children.
    foreach ($results as &$result) {
      if (!empty($result->getChildren())) {
        $children = $this->sortFacetResults($active_sort_processors, $result->getChildren());
        $result->setChildren($children);
      }
    }

    return $results;
  }

}
