<?php

/**
 * @file
 * Primary module hooks for Facet Active by Default module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */

use Drupal\facet_active_by_default\Plugin\facets\processor\ActiveByDefaultProcessor;
use Drupal\facets\Plugin\Block\FacetBlock;
use Drupal\facets\Processor\ProcessorInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function facet_active_by_default_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the facet_active_by_default module.
    case 'help.page.facet_active_by_default':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module is to be used in conjunction with the facets module. It makes the first facet in the list active by default.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_block_build_alter().
 */
function facet_active_by_default_block_build_alter(array &$build, \Drupal\Core\Block\BlockPluginInterface $block) {
  if ($block instanceof FacetBlock) {
    $facet_storage = \Drupal::entityTypeManager()->getStorage('facets_facet');
    $facet = $facet_storage->load($block->getDerivativeId());
    foreach ($facet->getProcessorsByStage(ProcessorInterface::STAGE_BUILD) as $processor) {
      if ($processor instanceof ActiveByDefaultProcessor) {
        $build['#create_placeholder'] = FALSE;
      }
    }
  }
}
